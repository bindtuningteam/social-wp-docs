### Title Link

If you provide a value in this field, when users click the web part's title bar, they will navigate to the provided URL.

___
### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.

![02.appearance.text.png](../images/modern/04.appearance.text.png)