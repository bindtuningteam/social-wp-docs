1. Open the page where you want to add the web part and click on the **Edit** button;
    
    ![edit-page](../images/modern/adding/01.edit-page.png)
    
2. Click on the **[+]** button to add a new web part or section to your page;

    ![add-section](../images/modern/adding/02.add-section.png)


3. On the web part panel, search for **BindTuning** to filter the web parts. Click on the **Social Web Part**;
    
	![modern-part](../images/modern/adding/03.modern-part.png)

