### UserName

This should be the page's Twitter handler without the @ symbol. 

![footer](../images/classic/twitter/01.username.png)


### Show Header

When enabled, a header will be added at the top of your feed with a link to the Twitter feed and a link to the [Twitter Ads Info and Privacy page](https://help.twitter.com/en/twitter-for-websites-ads-info-and-privacy)

![header](../images/classic/twitter/02.header.png)

### Show Footer

When enabled, a footer will be added at the end of the feed with an option to embed it somewhere else and an option to view it on Twitter

![footer](../images/classic/twitter/03.footer.png)

### Max Tweets

Set how many Tweets you'd like to show at any one time. The latest Tweets will be retrieved.