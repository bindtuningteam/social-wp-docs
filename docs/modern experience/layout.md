### Max Width
(optional for Twitter and Instagram)

By default, the feed will take up as much space as there is avaliable in the page.
By setting a maximum width value, the feed will never take up more space than what is defined.

![max-width](../images/classic/layout/01.max-width.png)

### Height
The height in pixels that the feed should take up on the page. If the content doesn't fit, a verticall scroll bar will appear

### Drop Shadow
If enabled, a drop shadow will be added to the feed, giving it the look of a card.

![drop-shadow](../images/classic/layout/02.drop-shadow.png)