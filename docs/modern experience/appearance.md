
### Right to Left

Using this option will change the web part's text orientation to go from right to left. <br>The forms will not be affected
