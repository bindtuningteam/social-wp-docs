1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **Social** web part, you can skip this process. 

    ![settings_edit.png](../images/msteams/setting_edit.png)
   
2. Configure the web part according to the settings described in the **[Web Part Properties Section](./general.md)**;

    ![02.properties-panel.png](../images/modern/configuring/02.properties-panel.png)

3. The properties are saved automatically, so when you're done click to close the Properties. 