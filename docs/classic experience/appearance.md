
### Chrome Type

In SharePoint, the Chrome is the title and border surrounding the content of a web part. This option allows you to set how and if this chrome will be displayed.

- **Default** - No Title and no Border will appear
- **None** - Same as **Default**.
- **Title and Border** - The web part will display the [title](#web-part-title) and a border
- **Title Only** - The web part will display the [title](#web-part-title)
- **Border Only** - The web part will be outlinesby a border


### Web Part Title

The title of the web part. This will show depending on the [Chrome Type](#chrome-type) option.

![title](../images/classic/appearance/01.title.png)

### Web Part URL

If the title is visble, clicking it will navigate to the provided URL. If a value isn't set, clicking the title won't navigate anywhere.

### Web Part Tooltip

If the title is visible, hovering it will cause a tooltip to appear after a short while. The content provided here will be added to the tooltip

![tooltip](../images/classic/appearance/02.tooltip.png)

### Right to Left

Using this option will change the web part's text orientation to go from right to left. <br>The forms will not be affected
