### Social Network

Select which social network you want to use.<br>
The panel will adjust to your pick.

![bindtuning-tab](../images/classic/general/01.general-options.png)

The following sections describe the options for each network

- [Configuring Yammer](./yammer.md)
- [Configuring Twitter](./twitter.md)
- [Configuring Facebook](./facebook.md)
- [Configuring Instagram](./instagram.md)
- [Configuring Pinterest](./pinterest.md)