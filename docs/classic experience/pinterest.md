### Profile Name

This will identify the Pinterest profile you want to showcase on the web part.<br>
To get the profile name, follow the steps below:

1. Open the pinterest page of choice

    ![pinterest-page](../images/classic/pinterest/01.pinterest-page.png)

2. Look to the url and find the URL component after `pinterest.com/`. Copy it

    ![username](../images/classic/pinterest/02.username.png)

3. Paste the user name into the form.

    ![username-form](../images/classic/pinterest/03.username-form.png)