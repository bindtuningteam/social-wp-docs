### UserName

This will indentify the facebook feed you'd like to see displayed.<br>
You can find the username right after the first forward slash in the page's url<br>
Make sure the page is public or nothing will be retrieved

![username](../images/classic/facebook/01.username.png)

### Events

When enabled, the Events tab will show above the feed

![events-tab](../images/classic/facebook/02.events-tab.png)

### Compact Header

When enabled, the header will be smaller leaving more room for content

![compact-header](../images/classic/facebook/03.compact-header.png)

### Show Cover Photo

When enabled, the cover photo of the page will appear as the background for the header.

![cover-photo](../images/classic/facebook/04.cover-photo.png)

### Show Friend's Faces

When enabled, users will see which of their friends have liked the selected page

![friends-faces](../images/classic/facebook/05.friends-faces.png)