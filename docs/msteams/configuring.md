1. On the Tab click on the dropdown and then **Settings**. If is the first time adding the **News**, you can skip this process. 

     ![setting_edit.png](../images/msteams/setting_edit.png)

2. Configure the web part according to the settings described in the **[Web Part Properties Section](./general.md)**;

    ![properties-panel](../images/modern/configuring/02.properties-panel.png)

3. The properties are saved automatically, so when you're done, simply save or publish the page and the content will be saved.