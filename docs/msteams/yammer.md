<p style="background-color:#FEB4AE; border-radius:15px; text-align: center;">Warning: Starting 2021, Yammer will no longer be supported by BindTuning Social. If you are in SharePoint Modern, we recommend using SharePoint native web part Conversations or Highlights</p>

### Domain (optional)
Use this option if you want to connect to a custom Yammer domain other than your default SharePoint's.

![domain](../images/classic/yammer/01.domain.png)

### Feed Filter
Lets you filter which content to diplay

- **No Filter** - Display a general view of the whole Yammer network
- **By Group** -  Only displays content created by a particular Yammer group
- **By Topic** -  Only displays content marked with the selected tag
- **By User** -  Only displays content made by the selected user


### Group ID
The ID of the group you want to filter by. To get the id, follow the steps below:

1. Open your Yammer network

    ![open-yammer](../images/classic/yammer/03.open-yammer.png)

3. Navigate to the group you want to filter by

    ![yammer-groups](../images/classic/yammer/04.yammer-groups.png)

4. Look at the url and find the `&feedId=0000000` component

    ![group-id](../images/classic/yammer/05.group-id.png)

5. Copy the number into the **Group ID** field on the Web Part's form

    ![group-id-form](../images/classic/yammer/06.group-id-form.png)

### Topic ID
The ID of the topic you want to filter by. To get the id, follow the steps below:

1. Open your Yammer network

    ![open-yammer](../images/classic/yammer/03.open-yammer.png)

2. Search for the topic you want to filter by

    ![search-topic](../images/classic/yammer/07.search-topic.png)

3. If the topic is found, click on it   
4. Look at the url and find the `&feedId=0000000` component

    ![topic-id](../images/classic/yammer/08.topic-id.png)

5. Copy the number into the **Group ID** field on the Web Part's form

    ![topic-id-form](../images/classic/yammer/09.topic-id-form.png)

### User ID
The ID of the user you want to filter by. To get the id, follow the steps below:

1. Open your Yammer network

    ![open-yammer](../images/classic/yammer/03.open-yammer.png)

2. Search for the user you want to filter by

    ![search-user](../images/classic/yammer/10.search-user.png)

3. If the topic is found, click on it
4. Look at the url and find the `/users/0000000000` component

    ![user-id](../images/classic/yammer/11.user-id.png)

5. Copy the number into the **Group ID** field on the Web Part's form

    ![user-id-form](../images/classic/yammer/12.user-id-form.png)

### Theme
Lets you choose the visual presentation of the Yammer feed.

![theme](../images/classic/yammer/13.theme.png)

### Show Header
If enabled, a header will be displayed with the name of the network on display. If disabled, only the feed will appear.

![header](../images/classic/yammer/14.header.png)

### Show Footer
If enabled, the footer will be displayed, showcasing the logged in account and the option to log out.

![footer](../images/classic/yammer/15.footer.png)