### Theme Colors on the Title Bar

Enabling this option will make it so the web part's title bar inherits the background color from your theme or customization on the page. <br>This option should help in maintaining branding consistency with native SharePoint web parts.

### Right to Left

Using this option will change the web part's text orientation to go from right to left. <br>The forms will not be affected


![appearance.png](../images/msteams/appearance.png)