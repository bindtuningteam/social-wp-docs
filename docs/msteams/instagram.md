### Add Posts

Here you can set the ids of different instagram posts, each separated by commas.<br>
To retrieve a post's id, follow the steps below:

1. Open the instagram page where the post is

    ![insta-page](../images/classic/instagram/01.insta-page.png)

2. Click the image you'd like to showcase on the web part

    ![post-expanded](../images/classic/instagram/02.post-expanded.png)

3. Look at the URL and find the `/p/XXXXXXXXXXX/` component and copy what comes after the `/p/`.

    ![post-id](../images/classic/instagram/03.post-id.png)

4. Paste the id into the form. If you already have an id, separate the two witha a comma.

    ![post-id-form](../images/classic/instagram/04.post-id-form.png)


